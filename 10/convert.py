import collections


def main():
    with open("input") as f:
        lines = [int(l.strip()) for l in f.readlines()]
    convert(lines)

    diffs = list(convert(lines))
    print(diffs)

    diff_lengths = list(convert_to_1_lengths(diffs))
    print(diff_lengths)

    poss = [possibilities(n) for n in diff_lengths]
    print(poss)

    result = 1
    for x in poss:
        result *= x

    print("Result: ", result)


def convert(lines):
    numbers = sorted(lines)

    count = collections.defaultdict(int)
    last = 0  # charging outlet joltage of seat
    for nb in numbers:
        diff = nb - last
        yield diff
        count[diff] += 1
        last = nb

    count[3] += 1  # joltage rating of phone
    yield 3


def convert_to_1_lengths(diffs):
    a = 0
    for diff in diffs:
        if diff == 1:
            a += 1
        elif diff == 3:
            if a > 0:
                yield a
            a = 0
        else:
            raise ValueError
    if a > 0:
        yield a


def possibilities(n):
    if n == 1:
        return 1
    if n == 2:
        return 2
    if n == 3:
        return 4
    if n == 4:
        return 7
    raise NotImplementedError


def test_convert():
    raw = [
        28,
        33,
        18,
        42,
        31,
        14,
        46,
        20,
        48,
        47,
        24,
        23,
        49,
        45,
        19,
        38,
        39,
        11,
        1,
        32,
        25,
        35,
        8,
        17,
        7,
        9,
        4,
        2,
        34,
        10,
        3,
    ]
    diffs = list(convert(raw))
    print(diffs)
    assert collections.Counter(diffs) == {1: 22, 3: 10}
    diff_lengths = list(convert_to_1_lengths(diffs))
    print(diff_lengths)
    poss = [possibilities(n) for n in diff_lengths]
    print(poss)

    result = 1
    for x in poss:
        result *= x
    assert result == 19208


if __name__ == "__main__":
    main()
