import collections


class Instruction(collections.namedtuple("Instruction", ("word", "nb"))):
    @classmethod
    def create(cls, line):
        word, nb = line.split(" ")
        return cls(word, int(nb))


def convert(lines):
    instructions = [Instruction.create(l) for l in lines]

    acc = 0
    pos = 0
    visited = set()
    while True:
        if pos in visited:
            break
        visited.add(pos)

        instruct = instructions[pos]
        print(instruct)
        if instruct.word == "nop":
            pos += 1
        elif instruct.word == "acc":
            acc += instruct.nb
            pos += 1
        elif instruct.word == "jmp":
            pos += instruct.nb

    return acc


def main():
    with open("input") as f:
        lines = [l.strip() for l in f.readlines()]
    result = convert(lines)
    print(f"result = {result}")


def test_instruc():
    raw = """nop +0
acc +1
jmp +4
acc +3
jmp -3
acc -99
acc +1
jmp -4
acc +6"""
    assert convert(raw.split("\n")) == 5


if __name__ == "__main__":
    main()
