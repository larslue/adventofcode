#!/usr/bin/env python
import re
from typing import Any, Literal, Optional, Type

import pydantic

ALL_KEYS = frozenset({"ecl", "pid", "eyr", "hcl", "byr", "iyr", "cid", "hgt"})
MUST_KEYS = frozenset(ALL_KEYS - set(["cid"]))


class Height(str):
    """hgt (Height) - a number followed by either cm or in:

    If cm, the number must be at least 150 and at most 193.
    If in, the number must be at least 59 and at most 76.
    """

    @classmethod
    def __get_validators__(cls):
        yield cls.validate

    @classmethod
    def validate(cls, v):
        if not isinstance(v, str):
            raise (TypeError("not a str"))
        unit = v[-2:]
        val = int(v[:-2])
        if unit == "cm":
            if not 150 <= val <= 193:
                raise TypeError("invalid cm")
        elif unit == "in":
            if not 59 <= val <= 76:
                raise TypeError("invalid cm")
        else:
            raise TypeError("neither cm or in")
        return cls(v)


class HairColor(str):
    """hcl (Hair Color) - a # followed by exactly six characters 0-9 or a-f."""

    regex = re.compile(r"^#[a-f0-9]{6}$")

    @classmethod
    def __get_validators__(cls):
        yield cls.validate

    @classmethod
    def validate(cls, v):
        if not isinstance(v, str):
            raise TypeError("not a str")
        m = cls.regex.fullmatch(v)
        if not m:
            raise TypeError("invalid format")
        return cls(v)


class PassportId(str):
    """pid (Passport ID) - a nine-digit number, including leading zeroes."""

    regex = re.compile(r"^\d{9}$")

    @classmethod
    def __get_validators__(cls):
        yield cls.validate

    @classmethod
    def validate(cls, v):
        if not isinstance(v, str):
            raise TypeError("not a str")
        m = cls.regex.fullmatch(v)
        if not m:
            raise TypeError("invalid format")
        return cls(v)


class Passport(pydantic.BaseModel):
    byr: pydantic.conint(ge=1920, le=2002)
    iyr: pydantic.conint(ge=2010, le=2020)
    eyr: pydantic.conint(ge=2020, le=2030)
    hgt: Height
    hcl: HairColor
    ecl: Literal["amb", "blu", "brn", "gry", "grn", "hzl", "oth"]
    pid: PassportId
    cid: Optional[Any]


def read_logs():
    with open("input") as f:
        return [l.strip() for l in f.readlines()]


def group_it(logs):
    pile = []
    for line in logs:
        if line == "":
            yield pile
            pile = []
        else:
            pile.append(line)
    yield pile


def passportize(lines):
    """Convert multiple lines to key, value pairs"""
    dct = {}
    for line in lines:
        for ele in line.split(" "):
            k, v = ele.split(":")
            dct[k] = v
    return dct


def is_valid(dct):
    missing_keys = MUST_KEYS - set(dct)
    return not missing_keys


def extended_validation(passp):
    try:
        p = Passport(**passp)
    except pydantic.ValidationError as exc:
        print(f"in={passp}, exc={exc}")
    else:
        print(f"out={p}, in={passp}")
        return p


def main():
    logs = read_logs()
    groups = group_it(logs)
    passports = [passportize(group) for group in groups]
    valid_passports = sum(is_valid(passport) for passport in passports)
    print(f"Of {len(passports)}, {valid_passports} are valid")

    pyd_passports = [extended_validation(dct) for dct in passports]
    print(f"Validated passports: ", len([a for a in pyd_passports if a]))


if __name__ == "__main__":
    main()


def test_passportize():
    raw = """ecl:gry pid:860033327 eyr:2020 hcl:#fffffd
byr:1937 iyr:2017 cid:147 hgt:183cm

iyr:2013 ecl:amb cid:350 eyr:2023 pid:028048884
hcl:#cfa07d byr:1929

hcl:#ae17e1 iyr:2013
eyr:2024
ecl:brn pid:760753108 byr:1931
hgt:179cm

hcl:#cfa07d eyr:2025 pid:166559648
iyr:2011 ecl:brn hgt:59in"""

    groups = group_it(raw.split("\n"))
    passports = [passportize(group) for group in groups]
    assert [is_valid(p) for p in passports] == [True, False, True, False]
    valid_passports = sum(is_valid(passport) for passport in passports)
    print(f"Of {len(passports)}, {valid_passports} are valid")
    assert valid_passports == 2


def test_ext_validation():
    valids = """pid:087499704 hgt:74in ecl:grn iyr:2012 eyr:2030 byr:1980
hcl:#623a2f

eyr:2029 ecl:blu cid:129 byr:1989
iyr:2014 pid:896056539 hcl:#a97842 hgt:165cm

hcl:#888785
hgt:164cm byr:2001 iyr:2015 cid:88
pid:545766238 ecl:hzl
eyr:2022

iyr:2010 hgt:158cm hcl:#b6652a ecl:blu byr:1944 eyr:2021 pid:093154719"""
    invalids = """eyr:1972 cid:100
hcl:#18171d ecl:amb hgt:170 pid:186cm iyr:2018 byr:1926

iyr:2019
hcl:#602927 eyr:1967 hgt:170cm
ecl:grn pid:012533040 byr:1946

hcl:dab227 iyr:2012
ecl:brn hgt:182cm pid:021572410 eyr:2020 byr:1992 cid:277

hgt:59cm ecl:zzz
eyr:2038 hcl:74454a iyr:2023
pid:3556412378 byr:2007"""

    groups = group_it(valids.split("\n"))
    passports = [passportize(group) for group in groups]
    for passp in passports:
        pydpass = extended_validation(passp)
        assert pydpass

    groups = group_it(invalids.split("\n"))
    passports = [passportize(group) for group in groups]
    for passp in passports:
        pydpass = extended_validation(passp)
        assert not pydpass


def test_hgt():
    def validate(val):
        try:
            return Height.validate(val)
        except:
            return False

    assert validate("150cm")
    assert not validate("149cm")
    assert validate("193cm")
    assert not validate("194cm")
    assert validate("59in")
    assert not validate("58in")
    assert validate("75in")
    assert not validate("77in")
    assert not validate("75")
    assert not validate("75rm")
