def find_weakness(lines):

    print("input len ", len(lines))

    preamble = 25

    def is_total(total, i, lines):
        for j, x in enumerate(lines[:i]):
            for k, y in enumerate(lines[:i]):
                if j != k:
                    # print(f"{x}+{y}={x+y} ?= {total}")
                    if x + y == total:
                        return True
        return False

    for i, total in enumerate(lines[preamble:]):
        index = i + preamble
        if is_total(total, index, lines):
            # print(f"{total} at {index} is fine")
            pass
        else:
            # print(f"{total} at {index} is not ok")
            return total


def is_any_total(lines, total):
    for i, l in enumerate(lines):
        if l == total:
            return i


def get_sum_range(lines, total):
    cache = list(lines)  # copy

    for offset in range(1, len(lines)):
        # add nb at i + offset to each element
        for i in range(len(lines) - offset):
            cache[i] += lines[i + offset]

        lower_bound = is_any_total(cache[: len(cache) - offset], total)
        if lower_bound is not None:
            return lower_bound, lower_bound + offset


def magic_number(lines, lb, ub):
    print(f"from {lb} to {ub}")
    min_nb = min(lines[i] for i in range(lb, ub + 1))
    max_nb = max(lines[i] for i in range(lb, ub + 1))
    print(f"weakness {min_nb} + {max_nb} = {min_nb+max_nb}")
    return min_nb + max_nb


def test_get_sum_range():
    # item is already in list
    assert get_sum_range([0, 1, 2], 2) is None
    # item is sum
    assert get_sum_range([1, 2, 4], 3) == (0, 1)
    # item is not in sum
    assert get_sum_range([1, 2, 3], 4) is None
    assert get_sum_range([1, 2, 3], 5) == (1, 2)
    assert get_sum_range([1, 2, 3], 6) == (0, 2)
    assert get_sum_range([1, 2, 4, 8], 14) == (1, 3)

    assert magic_number([1, 2, 4, 8], 0, 1) == 3
    assert magic_number([1, 2, 4, 8], 1, 3) == 10


def main():
    with open("input") as f:
        lines = [int(l.strip()) for l in f.readlines()]
    weakness = find_weakness(lines)
    print(f"{weakness} is not ok")
    lb, ub = get_sum_range(lines, weakness)
    result = magic_number(lines, lb, ub)


if __name__ == "__main__":
    main()
