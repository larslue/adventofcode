import re
import traceback as tb

import networkx

contain_reg = re.compile(r"^(.+) contain (.+)\.$")
source_bag_reg = re.compile(r"(\w+) (\w+) bags?")
target_bag_reg = re.compile(r"(\d) (\w+) (\w+) bags?")
shiny_gold = "shiny gold"


def extract_bags(bag_reg, bag_str):
    m = bag_reg.search(bag_str)
    if m:
        gr = m.groups()
        if len(gr) == 3:
            ct, pattern, color = gr
            return int(ct), " ".join((pattern, color))
        else:
            pattern, color = gr
            return " ".join((pattern, color))


def build_graph(lines):
    G = networkx.DiGraph()
    for line in lines:
        m = contain_reg.fullmatch(line)
        source = extract_bags(source_bag_reg, m.group(1))
        for raw in m.group(2).split(", "):
            tpl = extract_bags(target_bag_reg, raw)
            if tpl:
                ct, target = tpl
                G.add_edge(source, target, ct=ct)
    return G


def convert(lines):
    G = build_graph(lines)
    return predecessors(G)


def predecessors(G):
    seen_nodes = set()
    work_list = list(G.predecessors(shiny_gold))
    while work_list:
        node = work_list.pop(0)
        if node in seen_nodes:
            continue
        seen_nodes.add(node)
        work_list.extend(G.predecessors(node))

    return len(seen_nodes)


def successors(G, node=shiny_gold, multiplier=1):
    """Let's just hope we have no circles"""
    print(len(tb.format_stack()), node, multiplier)
    total = 1 * multiplier
    succs = list(G.successors(node))
    for succ in succs:
        ct = G.edges[node, succ]["ct"]
        total += successors(G, node=succ, multiplier=ct * multiplier)
    return total


def main():
    with open("input") as f:
        data = [l.strip() for l in f.readlines()]
    G = build_graph(data)
    result = predecessors(G)
    print("{} bags eventually contain a shiny gold bag".format(result))
    result = successors(G) - 1
    print("A shiny golden bag contains {} other bags".format(result))


def test_convert():
    raw = """light red bags contain 1 bright white bag, 2 muted yellow bags.
dark orange bags contain 3 bright white bags, 4 muted yellow bags.
bright white bags contain 1 shiny gold bag.
muted yellow bags contain 2 shiny gold bags, 9 faded blue bags.
shiny gold bags contain 1 dark olive bag, 2 vibrant plum bags.
dark olive bags contain 3 faded blue bags, 4 dotted black bags.
vibrant plum bags contain 5 faded blue bags, 6 dotted black bags.
faded blue bags contain no other bags.
dotted black bags contain no other bags."""
    G = build_graph(raw.split("\n"))
    result = predecessors(G)
    assert result == 4

    result = successors(G) - 1
    assert result == 32

    raw2 = """shiny gold bags contain 2 dark red bags.
dark red bags contain 2 dark orange bags.
dark orange bags contain 2 dark yellow bags.
dark yellow bags contain 2 dark green bags.
dark green bags contain 2 dark blue bags.
dark blue bags contain 2 dark violet bags.
dark violet bags contain no other bags."""

    result = successors(build_graph(raw2.split("\n"))) - 1
    assert result == 126


if __name__ == "__main__":
    main()
