#!/usr/bin/env python
def read_logs():
    with open("input") as f:
        logs = f.readlines()
    return logs


def to_bin(pos_str):
    return to_nb([to_bin_char(pos) for pos in pos_str.strip()])


def to_nb(float_lst):
    """in: 0100101, out: nb"""
    result = 0
    i = 0
    for nb in reversed(float_lst):
        result += nb * (2 ** i)
        i += 1
    return result


def to_bin_char(pos):
    if pos == "F":
        return 0
    if pos == "B":
        return 1
    if pos == "L":
        return 0
    if pos == "R":
        return 1
    raise ValueError(f"Illegal char {pos}")


def main():
    all_boarding_passes = read_logs()

    pass_ids = [to_bin(ticket) for ticket in all_boarding_passes]

    print("Max id: {}".format(max(pass_ids)))

    all_possible_ids = set(range(1024))

    missing_ids = all_possible_ids - set(pass_ids)

    print(f"Missing ids: {sorted(missing_ids)}")


if __name__ == "__main__":
    main()


def test_boarding_pass_conversion():
    assert to_bin("BFFFBBFRRR") == 567
    assert to_bin("FFFBBBFRRR") == 119
    assert to_bin("BBFFBBFRLL") == 820
