#!/usr/bin/env python
import itertools as it


def read_logs(filename="input"):
    with open(filename) as f:
        return f.readlines()


def group_it(logs):
    pile = []
    for log in logs:
        if log in ("\n", ""):
            yield pile
            pile = []
        else:
            pile.append(set(log.strip()))
    yield pile


def unionize(group):
    union = set()
    for person in group:
        union |= person
    return union


def intersection(group):
    res = set.intersection(*(person for person in group))
    return res


def main():
    logs = read_logs()

    groups = group_it(logs)
    union_groups = [unionize(group) for group in groups]
    print("Total count: ", sum(len(uni) for uni in union_groups))

    groups = group_it(logs)
    intersection_groups = [intersection(group) for group in groups]
    count = sum(len(uni) for uni in intersection_groups)
    print(f"Intersection count: {count}")


if __name__ == "__main__":
    main()


def test_main():
    raw = """abc

    a
    b
    c

    ab
    ac

    a
    a
    a
    a

    b"""
    groups = list(group_it(raw.split("\n")))
    assert len(groups) == 5
    union_groups = [unionize(group) for group in groups]
    count = sum(len(uni) for uni in union_groups)
    assert count == 11

    intersection_groups = [intersection(group) for group in groups]
    count = sum(len(uni) for uni in intersection_groups)
    assert count == 6
